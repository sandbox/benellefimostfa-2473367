
CKEDITOR_ACCORDION MODULE FOR DRUPAL 7.x
---------------------------

CONTENTS OF THIS README
-----------------------

   * Description
   * Requirements
   * Installation
   * Credits

DESCRIPTION
-----------

This module allow users to add accordion system to a node or a field


REQUIREMENTS
------------

This module require ckeditor module.

INSTALLATION
------------

1.Install the ckeditor module and enable it.

2.Instal and enable ckeditor_accordion module.

3.Goto: [admin/config/content/ckeditor/edit/Full] for full html option
 or [admin/config/content/ckeditor/edit/Advanced] for Filtred html option.

4.Goto: Editor appearance fieldset.

5.Enable the accordion plugin in plugins and drog and drop the accordion icon in the toolbar.


CREDITS
-------

7.x.1.0 developed by benellefimostfa
