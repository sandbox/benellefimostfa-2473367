Drupal.behaviors.ckeditor_accordion = {
  attach: function (context, settings) {
    (function ($) {
      $(document, context).ready(function() {
        $(".accordion-ckeditor ").each(function() {
          $(this).find("div:first").css("display","block");
          $(this).find("h1,h2,h3,h4,h5").click(function(){
            $(this).next().slideToggle(500);
            $(".accordion-ckeditor div").not(jQuery(this).next()).slideUp(500);
          });
        });
      });
      })(jQuery);
    }
};
