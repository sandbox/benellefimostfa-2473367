CKEDITOR.dialog.add( 'accordionDialog', function ( editor ) {
  return {
    title: 'Accordion Configuration',
      minWidth: 400,
      minHeight: 200,
      contents: [
        {
          id: 'tab-basic',
          label: 'Basic Settings',
          elements: [
            {
              type: 'text',
              id: 'number',
              label: 'Number of Sections of the accordion',
              validate: CKEDITOR.dialog.validate.notEmpty( "It can not be empty" )
            },
            {
              type : 'select',
              id : 'head',
              label : 'Sections titles',
              items :
                [
                  [ '<none>', '' ],
                  [ 'h1', 'h1' ],
                  ['h2', 'h2' ],
                  ['h3', 'h3' ],
                  ['h4', 'h4' ],
                  ['h5', 'h5' ],
                  ['h6', 'h6' ]
                ]
            }
          ]
        }
      ],
      onOk: function() {
        var dialog = this;
        var sections = parseInt(dialog.getValueOf('tab-basic','number')); //Number of sections that will be created
        var headers = dialog.getValueOf('tab-basic','head');//Value of section headers
        console.log(headers);
        section = '<'+headers+'>Section Name</'+headers+'><div><p>Enter the text of the accordion section here</p></div>'
        intern = ""
        for (i=0;i<sections;i++){
          intern = intern + section
        }
        editor.insertHtml('<!--break--><div class="accordion-ckeditor">'+ intern +'</div><!--break-->');
        }
  };
});

