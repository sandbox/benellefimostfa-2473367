CKEDITOR.plugins.add('accordion', {
  init: function (editor) {
    editor.addCommand( 'accordionDialog', new CKEDITOR.dialogCommand( 'accordionDialog' ) );
    editor.ui.addButton('accordion', {
      label: 'accordion',
      command: 'accordionDialog',
      icon: this.path +'accordion.png'
    });
    CKEDITOR.dialog.add( 'accordionDialog', this.path + 'dialogs/accordion.js' );
  }
});
